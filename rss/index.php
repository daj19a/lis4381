<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Assignment 4 Website.">
	<meta name="author" content="Daniel Jaramillo">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - RSS</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>
	<?php include_once("../global/nav.php"); ?>
	<?php
	$html = "";
	$publisher = "Washington Post";
	$url = "https://feeds.washingtonpost.com/rss/business/technology";

	$html .= '<h2>' . $publisher . '<h2>';
	$html .= $url;

	$rss = simplexml_load_file($url);
	$count = 0;
	$html .= '<ul>';
	foreach($rss->channel->item as $item)
	{
		$count++;
		if($count > 10)
		{
			break;
		}
		$html .= '<li><a href="'.htmlspecialchars($item->link).'">'. htmlspecialchars($item->title) . '</a><br />';
		$html .= htmlspecialchars($item->description) . '<br />';
		$html .= htmlspecialchars($item->pubDate) . '</li><br />';
	}
	$html .= '</ul>';
	print $html;
	?>

</body>
</html>
