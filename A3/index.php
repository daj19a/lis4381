<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Assignment 4 Website.">
	<meta name="author" content="Daniel Jaramillo">
    <link rel="icon" href="favicon.png" size="16x16">

	<title>LIS4381 - Assignment3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Students were expected to develop and forward engineer a MySQL database, run an Android application to calculate ticket prices called "My Event", and complete skillsets 4-6. 
				</p>

				<h4>MySQL ERD</h4>
				<img src="Images/sqlerd.png" class="img-responsive center-block" alt="MySQL ERD">
				<h4>Android Studio App "My Event"</h4>
				<video width="800" controls>
					<source src="Images/myeventVideo.mp4" type="video/mp4">
				Your browser does not support the video tag.
				</video>

				<h4>Skillset 4</h4>
				<img src="Images/skillset4.png" class="img-responsive center-block" alt="Skillset 4">

				<h4>Skillset 5</h4>
				<img src="Images/skillset5.png" class="img-responsive center-block" alt="Skillset 5">

				<h4>Skillset 6</h4>
				<img src="Images/skillset6.png" class="img-responsive center-block" alt="Skillset 6">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
