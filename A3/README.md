> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Daniel Jaramillo

### Assignment 3 Requirements:

1. Develop and forward engineer a MySQL database.
2. Successfully compile and run an application that can calculate ticket prices called "My Event."
2. Complete Skillsets 4-6.

#### README.md file should include the following items:

* Screenshot of ERD.
* Links to a3.mwb and a3.sql.
* Screenshot of ten records for each table in the database.
* Running application's opening interface and processing user input.

#### Assignment Screenshots:

My Event Application: https://youtu.be/zx2RqDnxygk 

MySQL .mwb : [A3.mwb](Docs/A3.mwb "A3.mwb")
MySQL .sql : [A3.sql](Docs/A3.sql "A3.sql")


MySQL ERD:
![MySQL ERD](Images/sqlerd.png)

SQL Record 1:
![SQL Records 1](Images/sqlrecords1.png)
SQL Record 2:
![SQL Records 2](Images/sqlrecords2.png)
SQL Record 3:
![SQL Records 3](Images/sqlrecords3.png)

| Skillset 4 | Skillset 5 | Skillset 6 |
| --- | --- | --- |
| ![Skillset 4](Images/skillset4.png) | ![Skillset 5](Images/skillset5.png) | ![Skillset 6](Images/skillset6.png) |
| --- | --- | --- |