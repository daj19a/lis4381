> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Daniel Jaramillo

### Project 1 Requirements:

1. Develop "My Business Card" Application
2. Create a launcher icon image and display it in both activities (screens) 
3. Add background color(s) to both activities 
4. Add border around image and button 
5. Add text shadow (button) 

#### README.md file should include the following items:

* Screenshot of first user interface
* Screenshot of second user interface
* Skillsets 7-9 

#### Assignment Screenshots:

| Skillset 7 | Skillset 8 | Skillset 9 |
| --- | --- | --- |
| ![Skillset 7](Images/skillset7.png) | ![Skillset 8](Images/skillset8.png) | ![Skillset 9](Images/skillset9.png) |
| --- | --- | --- |
"My Business Card" User Interface 1:
![My Business Card Screen 1](Images/ui1.png)
<br />
"My Business Card" User Interface 2:
![My Business Card Screen 2](Images/ui2.png)

