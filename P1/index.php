<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Assignment 4 Website.">
		<meta name="author" content="Daniel Jaramillo">
    <link rel="icon" href="favicon.png">

		<title>LIS4381 - Assignment3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Students were expected to develop an Android Studio business card application and complete skillsets 7-9. 
				</p>

				<h4>Android Studio App User Interface 1</h4>
				<img src="Images/ui1.png" class="img-responsive center-block" alt="User Interface 1">

				<h4>Android Studio App User Interface 2</h4>
				<img src="Images/ui2.png" class="img-responsive center-block" alt="User Interface 2">

				<h4>Skillset 7</h4>
				<img src="Images/skillset7.png" class="img-responsive center-block" alt="Skillset 7">

				<h4>Skillset 8</h4>
				<img src="Images/skillset8.png" class="img-responsive center-block" alt="Skillset 8">

				<h4>Skillset 9</h4>
				<img src="Images/skillset9.png" class="img-responsive center-block" alt="Skillset 9">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
