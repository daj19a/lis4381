> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Daniel Jaramillo

### Assignment 5 Requirements:

1. Successfully set-up LIS4381 Assignment 5 Page
2. Connect mySQL database to webpage
3. Add server side validation to form
4. Have data entered into form update the database
5. Complete Skillsets 13-15

#### README.md file should include the following items:

* Screenshots of A5 Webpage
* Failed validation for A5
* Successful validation for A5
* Skillsets 13-15

#### Assignment Screenshots:


Website Link [Here.](http://localhost/repos/lis4381/index.php)

| Failed Validation 1 | Failed Validation 2 |
| --- | --- |
| ![Failed Validation 1](img/failed1.png) | ![Failed Validation 2](img/failed2.png) |

| Successful Validation 1 | Successful Validation 2 |
| --- | --- |
| ![Successful Validation 1](img/valid1.png) | ![Successful Validation 2](img/valid2.png) |

### Skillset 13:
![Skillset 13](img/skillset13.png)
### Skillset 14:
| Successful Validation 1 | Successful Validation 2 |
| --- | --- |
| ![Successful Validation 1](img/skillset14_a.png) | ![Successful Validation 2](img/skillset14_b.png) |

| Failed Validation 1 | Failed Validation 2 |
| --- | --- |
| ![Failed Validation 1](img/skillset14_c.png) | ![Failed Validation 2](img/skillset14_d.png) |

### Skillset 15:
| Image 1 | Image 2 |
| --- | --- |
| ![Image 1](img/skillset15_a.png) | ![Image 2](img/skillset15_b.png) |