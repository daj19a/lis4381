> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Daniel Jaramillo

### Assignment 1 Requirements:

1. Complete BitBucketStations tutorial
2. Install Ampps
3. Install JDK and run World.java
4. Complete Android Studios tutorial

#### README.md file should include the following items:

* Screenshot of ampps installation running 
* Screenshot of running JDK java Hello 
* Screenshot of running Android Studio My First App
* git commands w/short descriptions (“Lesson 3b - Version Control Systems: Course Configuration”); 
* Bitbucket repo links:  

> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git log - See the repository’s history by listing certain commit’s details

#### Assignment Screenshots:

| Screenshot of AMPPS running | Screenshot of Java Hello | Screenshot of Android Studio |
| --- | --- | --- |
| ![AMPPS Installation Screenshot](Images/Ampps.png) | ![JDK Installation Screenshot](Images/Java.png) | ![Android Studio Installation Screenshot](Images/AndroidStudio.png) |
| --- | --- | --- |


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/daj19a/bitbucketstationlocations "Bitbucket Station Locations")
