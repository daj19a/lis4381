# LIS4381 - Mobile Web Application Development

## Daniel Jaramillo

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](A1/README.md "My A1 README.md file")
    - Complete BitBucketStations tutorial
    - Install Ampps
    - Install JDK and run World.java
    - Complete Android Studios tutorial

2. [A2 README.md](A2/README.md "My A2 README.md file")
    - Complete Healthy Recipes Application in Android Studios
    - Successfully compile and run the application
    - Complete Skillsets 1-3

3. [A3 README.md](A3/README.md "My A3 README.md file")
    - Develop and forward engineer a MySQL database
    - Successfully compile and run an application that can calculate ticket prices called "My Event."
    - Complete Skillsets 4-6
4. [P1 README.md](P1/README.md "My P1 README.md file")
    - Develop "My Business Card" Application
    - Create a launcher icon image and display it in both activities (screens) 
    - Add background color(s) to both activities 
    - Add border around image and button 
    - Add text shadow (button)
4. [A4 README.md](A4/README.md "My A4 README.md file")
    - Successfully set-up LIS4381 Portal Page
    - Set up individual assignment pages to reflect assignment details
    - Update carousel on home page to reflect developer
    - Add jQuery validation and regular expression attributes to Assignment 4
    - Complete Skillsets 10-12
5. [A5 README.md](A5/README.md "My A5 README.md file")
    - Successfully set-up LIS4381 Assignment 5 Page
    - Connect mySQL database to webpage
    - Add server side validation to form
    - Have data entered into form update the database
    - Complete Skillsets 13-15
6. [P2 README.md](P2/README.md "My P2 README.md file")
    - Successfully set-up LIS4381 Project 2 Webpage
    - Add Edit and Delete Functionality
    - Successfully Set Up RSS Feed Page
