> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Daniel Jaramillo

### Project 2 Requirements:

1. Successfully set-up LIS4381 Project 2 Webpage
2. Add Edit and Delete Functionality
3. Successfully Set Up RSS Feed Page

#### README.md file should include the following items:

* Index.php for P2
* Failed Editing for P2 with Error Page
* Successful Editing for P2
* Delete Record Prompt and Success
* RSS Feed

#### Assignment Screenshots:


Website Link [Here.](http://localhost/repos/lis4381/index.php)

Index.php Before Editing: ![Index.php Before Editing](img/index_pre_edit.png)

| Failed Edit 1 | Failed Edit 2 |
| --- | --- |
| ![Failed Validation 1](img/failed_edit1.png) | ![Failed Validation 2](img/failed_edit2.png) |

| Successful Edit 1 | Successful Edit 2 |
| --- | --- |
| ![Successful Validation 1](img/success_edit1.png) | ![Successful Validation 2](img/success_edit2.png) |

| Delete 1 | Delete 2 |
| --- | --- |
| ![Delete 1](img/delete1.png) | ![Delete 2](img/delete2.png) |

RSS Feed: ![RSS Feed](img/rss.png)