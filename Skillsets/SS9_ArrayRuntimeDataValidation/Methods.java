import java.util.Scanner;
import java.math.RoundingMode;
import java.text.DecimalFormat;
public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Daniel Jaramillo");
        System.out.println("1) Program creates array size at run-time.");
        System.out.println("2) Program displays array size.");
        System.out.println("3) Program rounds sum and average of numbers to two decimal places.");
        System.out.println("Numbers *must* be float data type, not double.");
    }
    public static void arraySize()
    {
        Scanner sc = new Scanner (System.in);
        //array size
        int j = 0;
        int arrayLength = 0;
        
        do
        {
            System.out.print("Please enter array size: ");
            if (sc.hasNextInt()) 
            {
                arrayLength = sc.nextInt();
                j = 1;
            }
            else if (sc.hasNextInt() == false)
            {
                System.out.print("Not a valid integer! \n\nPlease try again. ");
                sc = new Scanner (System.in);
            }
        }
        while (j == 0);
        int i = 1;
        System.out.println("Please enter " + arrayLength + " number(s).");
        float[] arrayAvg;
        arrayAvg = new float[arrayLength];
        do {
            System.out.print("Enter num " + i + ": ");
            if (sc.hasNextFloat()) 
            {
                arrayAvg[i-1] = sc.nextFloat();
                i++;
            }
            else if (sc.hasNextInt() == false)
            {
                System.out.print("Not a valid number! \n\nPlease try again. ");
                sc = new Scanner (System.in);
            }
        }while (i <= arrayLength);
        //output user input
        System.out.print("Numbers entered: ");
        for (float k: arrayAvg) {
            System.out.print(k + ", ");
        }
        //sum
        float sum = 0;
        for (float l: arrayAvg) {
            sum = sum + l;
        }
        System.out.println("Sum: " + sum);
        //avg
        float avg = sum / arrayLength;
        DecimalFormat df = new DecimalFormat("0.00");
        System.out.println("Average: " + df.format(avg));
    }
}