import java.util.Scanner;
public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Daniel Jaramillo");
        System.out.println("Program loops through array of strings");
        System.out.println("Use following loop structures: for, enhanced for, while, do...while.");
        System.out.println();
    }
    public static void arrayTime()
    {
        //initialize array
        String[] animalArray;
        animalArray = new String[5];
        animalArray[0] = "dog";
        animalArray[1] = "cat";
        animalArray[2] = "bird";
        animalArray[3] = "fish";
        animalArray[4] = "insect";
        //run the loops
        System.out.println("for loop:");
        for (int i = 0; i<5; i++) 
        {
            System.out.println(animalArray[i]);
        }
        System.out.println();
        System.out.println("Enhanced for loop:");
        for (String i: animalArray)
        {
            System.out.println(i);
        }
        System.out.println();
        System.out.println("while loop:");
        int j = 0;
        while (j < 5)
        {
            System.out.println(animalArray[j]);
            j = j+1;
        }
        System.out.println();
        System.out.println("do...while loop:");
        int g = 0;
        do
        {
            System.out.println(animalArray[g]);
            g = g+1;
        }
        while (g<5);
    }
}