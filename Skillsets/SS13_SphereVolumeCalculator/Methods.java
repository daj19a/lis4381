import java.util.Scanner;
import java.math.RoundingMode;
import java.text.DecimalFormat;
public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Daniel Jaramillo");
        System.out.println("Sphere Volume Program\n");
        System.out.println("Program calculates sphere volume in liquid U.S gallons from user-entered diameter value in inches and rounds to two decimal places.");
        System.out.println("Must use Java's built in PI and pow() capabilities.");
        System.out.println("Program checks for non-integers and non-numeric values.\nProgram continues to prompt for user entry until no longer requested, prompt accepts upper or lower case values.");
    }
    public static void sphereCalc()
    {
        Scanner sc = new Scanner (System.in);
        int diameter = 0;
        char userInput = ' ';
        DecimalFormat df = new DecimalFormat("0.00");
        int i=0;
        int j = 0;
        double radiusCubed = 0.0;
        double sphereVolume = 0.0;
        do
        //j loop
        {
            do 
            //i loop
            {
                System.out.print("Please enter diameter in inches: ");
                if (sc.hasNextInt()) 
                {
                    diameter = sc.nextInt();
                    i = 1;
                }
                else if (sc.hasNextInt() == false)
                {
                    System.out.print("Not a valid integer! \n\nPlease try again. ");
                    sc = new Scanner (System.in);
                }
            } while (i == 0);
            //sphere formula: (4/3) * pi * r^3
            radiusCubed = Math.pow((diameter / 2.0), 3.0);
            sphereVolume = (radiusCubed * (4.0/3.0) * Math.PI);
            sphereVolume = sphereVolume / 231.0;
            System.out.println("\nSphere Volume: " + df.format(sphereVolume) + " liquid U.S gallons");
            System.out.print("Do you want to calculate another sphere volume (Y or N): ");
            userInput = sc.next().charAt(0);
            if (userInput == 'n' || userInput == 'N') 
            {
                System.out.println("\nThank you for using our Sphere Volume Calculator!");
                j = 1;    
            }
        }
        while (j == 0);
    }
}