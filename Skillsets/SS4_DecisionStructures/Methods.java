import java.util.Scanner;
public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Daniel Jaramillo");
        System.out.println("Program evaluates user-entered characters.");
        System.out.println("Use following characters: W or w, C or c, H or H, N or n.");
        System.out.println("Use Following decision structures: if...else, and switch.");
        System.out.println();
    }
    public static void phoneCheck()
    {
        System.out.println("Phone Types: W or w (work), C or c (cell), H or h (home), N or n (none)");
        System.out.println("Enter phone type: ");
        Scanner sc = new Scanner(System.in);
        char phone=' ';
        phone = sc.next().charAt(0);
        //if else
        System.out.println("if...else:");
        if (phone == 'W' || phone == 'w')
        {
            System.out.println("Phone type: work");
        }
        else if (phone == 'C' || phone == 'c')
        {
            System.out.println("Phone type: cell");
        }
        else if (phone == 'H' || phone == 'h')
        {
            System.out.println("Phone type: home");
        }
        else if (phone == 'N' || phone == 'n')
        {
            System.out.println("Phone type: none");
        }
        else
        {
            System.out.println("Invalid character entry");
        }
        //switch
        System.out.println("Switch");
        switch(phone) 
        {
            case 'W': System.out.println("Phone type: work");
            break;
            case 'w': System.out.println("Phone type: work");
            break;
            case 'C': System.out.println("Phone type: cell");
            break;
            case 'c': System.out.println("Phone type: cell");
            break;
            case 'H': System.out.println("Phone type: home");
            break;
            case 'h': System.out.println("Phone type: home");
            break;
            case 'N': System.out.println("Phone type: none");
            break;
            case 'n': System.out.println("Phone type: none");
            break;
            default: System.out.println("Invalid character entry");
            break;
        }

        //close the scanner
        sc.close();
    }
}