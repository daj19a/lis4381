import java.util.Scanner;
import java.util.Random;
public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Daniel Jaramillo");
        System.out.println("Program evaluates largest of three integers.");
        System.out.println("Note: Program checks for integers and non-numeric values");
    }
    public static void largest()
    {
        Scanner sc = new Scanner (System.in);
        int num1 = 0;
        int num2 = 0;
        int num3 = 0;
        

        //number 1
        int j = 0;
        do
        {
            System.out.print("Enter the first number: ");
            if (sc.hasNextInt()) 
            {
                num1 = sc.nextInt();
                j = 1;
            }
            else if (sc.hasNextInt() == false)
            {
                System.out.print("Not a valid integer! \n\nPlease try again. ");
                sc = new Scanner (System.in);
            }
        }
        while (j == 0);

        //number 2
        j = 0;
        do
        {
            System.out.print("Enter the second number: ");
            if (sc.hasNextInt()) 
            {
                num2 = sc.nextInt();
                j = 1;
            }
            else if (sc.hasNextInt() == false)
            {
                System.out.print("Not a valid integer! \n\nPlease try again. ");
                sc = new Scanner (System.in);
            }
        }
        while (j == 0);

        //number 3
        j = 0;
        do
        {
            System.out.print("Enter the third number: ");
            if (sc.hasNextInt()) 
            {
                num3 = sc.nextInt();
                j = 1;
            }
            else if (sc.hasNextInt() == false)
            {
                System.out.print("Not a valid integer! \n\nPlease try again. ");
                sc = new Scanner (System.in);
            }
        }
        while (j == 0);        
        
        //check for largest number
        if (num1 > num2 && num1 > num3) 
        {
            System.out.println("First number is the largest.");
        }
        else if (num2 > num1 && num2 > num3)
        {
            System.out.println("Second number is the largest.");
        }
        else 
        {
            System.out.println("Third number is the largest");
        }
    }
}