import java.util.Scanner;
import java.math.RoundingMode;
import java.text.DecimalFormat;
public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Daniel Jaramillo");
        System.out.println("Temperature Conversion Program\n");
        System.out.println("Program converts user-entered temperatures into Farenheit or Celsius scales.");
        System.out.println("Program continues to prompt for user entry until no longer requested.");
        System.out.println("Note: upper or lower case letters permitted. Incorrect entries are not permitted.\nNote: Program does not validate numeric data (optional requirement).\n");
    }
    public static void tempConv()
    {
        Scanner sc = new Scanner (System.in);
        char userInput = ' ';
        double userTemp = 0.0;
        double convertedTemp = 0.0;
        DecimalFormat df = new DecimalFormat("0.0");
        int j = 0;
        
        do
        {
            System.out.print("\nFahrenheit to Celsius? Type \"F\", or Celsius to Fahrenheit? Type \"c\": ");
            userInput = sc.next().charAt(0);
            if (userInput == 'f' || userInput == 'F') 
            {
                System.out.print("\nEnter temperature in Fahrenheit: ");
                userTemp = sc.nextDouble();
                convertedTemp = (userTemp - 32) / 1.8;
                System.out.println("Temperature in Celsius = " + df.format(convertedTemp));
            }
            else if (userInput == 'c' || userInput == 'C')
            {
                System.out.print("\nEnter temperature in Celsius: ");
                userTemp = sc.nextDouble();
                convertedTemp = (userTemp * 1.8) + 32;
                System.out.println("Temperature in Fahrenheit = " + df.format(convertedTemp));
            }
            else
            {
                System.out.println("Incorrect entry. Please try again.");
            }
            System.out.print("Do you want to convert a temperature (y or n)? ");
            userInput = ' ';
            userInput = sc.next().charAt(0);
            if (userInput == 'n' || userInput == 'N') 
            {
                System.out.println("\nThank you for using our Temperature Conversion Program!");
                j = 1;    
            }
        }
        while (j == 0);
    }
}