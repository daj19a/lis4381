import java.util.Scanner;
import java.util.Random;
public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Daniel Jaramillo");
        System.out.println("Print minimum and maximum values.");
        System.out.println("Program prompts user to enter desired number of pseudorandom-generated integers.");
        System.out.println("Use following loop strucutres: for, enhanced for, while, do...while.");
        System.out.println();
        System.out.println("Integer.MIN_VALUE = " + Integer.MIN_VALUE);
        System.out.println("Integer.MAX_VALUE = " + Integer.MAX_VALUE);
    }
    public static int[] createArray()
    {
        Scanner sc = new Scanner (System.in);
        int arraySize = 0;
        System.out.println("Enter the desired number of pseudorandom integers (min 1)");
        arraySize = sc.nextInt();
        sc.close();
        int yourArray[] = new int[arraySize];
        return yourArray;
    }

    public static void rng(int[] userArray)
    {
        Random r = new Random();
        int i = 0;
        System.out.println("\nfor loop:");
        for(i=0; i<userArray.length; i++)
        {
            System.out.println(r.nextInt());
        }
        System.out.println("\nenhanced for loop:");
        for(int n: userArray)
        {
            System.out.println(r.nextInt());
        }
        i = 0;
        System.out.println("\nwhile loop:");
        while (i < userArray.length)
        {
            System.out.println(r.nextInt());
            i++;
        }
        i = 0;
        System.out.println("\ndo...while loop:");

        do
        {
            System.out.println(r.nextInt());
            i++;
        }
        while (i<userArray.length);
        
    }
}