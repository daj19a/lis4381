import java.util.Scanner;
public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Daniel Jaramillo");
        System.out.println("Program evaluates whether an integer is even or odd.");
        System.out.println("Program does *not* check for non-numeric characters");
        System.out.println();
    }
    public static void evaluateNumber()
    {
        int value = 0;
        System.out.println("Enter integer: ");
        Scanner sc = new Scanner(System.in);
        value = sc.nextInt();
        if (value % 2 == 0)
        {
            System.out.println(value + " is an even number.");
        }
        else
        {
            System.out.println(value + " is an odd number.");
        }
    }
}