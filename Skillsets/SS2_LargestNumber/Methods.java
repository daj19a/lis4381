import java.util.Scanner;
public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Daniel Jaramillo");
        System.out.println("Program evaluates which integer is the largest");
        System.out.println("Program does *not* check for non-numeric characters");
        System.out.println();
    }
    public static void evaluateNumber()
    {
        int value1 = 0;
        int value2 = 0;
        System.out.println("Enter first integer: ");
        Scanner sc = new Scanner(System.in);
        value1 = sc.nextInt();
        System.out.println("Enter second integer: ");
        value2 = sc.nextInt();
        sc.close();
        if (value1 > value2)
        {
            System.out.println(value1 + " is greater than " + value2 + ".");
        }
        else if (value2 > value1)
        {
            System.out.println(value2 + " is greater than " + value1 + ".");
        }
        else
        {
            System.out.println("Integers are equal.");
        }
    }
}