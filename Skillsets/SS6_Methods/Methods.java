import java.util.Scanner;
public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Daniel Jaramillo");
        System.out.println("Program prompts user for first name and age, then print results.");
        System.out.println("Create four methods from the following requirements.");
        System.out.println("1) getRequirements(): Void method displays program requirements.\n2) getUserInput(): Void method prompts for user input, then calls 2 methods: myVoidMethod() and myValueReturningMethod().\n3) myVoidMethod(): Accepts two arguments: String and int. Prints user's first name and age.\n4) myValueReturningMethod(): Accepts two arguments: String and int. Returns string containing first name and age.");
        System.out.println();
    }
    public static void getUserInput()
    {
        System.out.println("Enter first name:");
        Scanner sc = new Scanner(System.in);
        String name = "";
        name = sc.nextLine();
        System.out.println("Enter age:");
        int age = 0;
        age = sc.nextInt();
        sc.close();
        System.out.println("\nVoid Method:");
        myVoidMethod(name, age);
        System.out.println("\nValue Returning Method:");
        System.out.println(myValueReturningMethod(name, age));
    }
    public static void myVoidMethod(String name, int age)
    {
        System.out.println(name + " is " + age);
    }
    public static String myValueReturningMethod(String name, int age)
    {
        String nameAndAge = (name + " is " + age);
        return nameAndAge;
    }
    //ill figure it out later i cant code while listening to him lol
}