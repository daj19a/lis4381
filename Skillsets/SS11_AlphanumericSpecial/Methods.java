import java.util.Scanner;
public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Daniel Jaramillo");
        System.out.println("Program determines whether user-entered value is alpha, numeric, or special character.");
        System.out.println("\nReferences:\nASCII Background: https://en.wikipedia.org/wiki/ASCII");
        System.out.println("ASCII Character Table: https://www.ascii-code.com/");
        System.out.println("Lookup Tables: https://www.lookuptables.com/ \n");
    }
    public static void alphaNumeric()
    {
        Scanner sc = new Scanner (System.in);
        char userInput = ' ';
        System.out.print("Enter character: ");
        userInput = sc.next().charAt(0);
        //#s < upper case < lower case
        if ((userInput >= 'a' && userInput <= 'z') || (userInput >= 'A' && userInput <= 'Z'))
        {
            System.out.println(userInput + " is alpha. ASCII value: " + (int) userInput);
        }
        else if (userInput >= '0' && userInput <= '9')
        {
            System.out.println(userInput + " is numeric. ASCII value: " + (int) userInput);
        }
        else
        {
            System.out.println(userInput + " is a special character. ASCII value: " + (int) userInput);
        }
        
    }
}