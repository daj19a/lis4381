import java.util.Scanner;
import java.util.ArrayList;
public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Daniel Jaramillo");
        System.out.println("Program populates ArrayList of strings with user-entered animal type values.");
        System.out.println("Examples: Polar bear, Guinea Pig, dog, cat, bird");
        System.out.println("Program continues to collect user-entered values until user types 'n' ");
        System.out.println("Program displays ArrayList values after each iteration, as well as size.");
    }
    public static void arraySize()
    {
        Scanner sc = new Scanner (System.in);
        Scanner sc2 = new Scanner (System.in);
        //initialize variables
        char userInput = ' ';
        ArrayList<String> animal = new ArrayList<String>();
        int i = 0;
        do
        {
            //enter array element
            System.out.print("Enter animal type: ");
            animal.add(sc.nextLine());
            //display array elements
            System.out.print("ArrayList elements: [");
            for (i=0; i < animal.size(); i++) 
            {
                System.out.print(animal.get(i));
                if (i<animal.size() - 1) {
                    System.out.print(", ");
                }
            }
            System.out.println("]");
            //display array size
            System.out.println("ArrayList Size = " + animal.size());
            //ask to continue
            System.out.println();
            System.out.print("Continue? Enter y or n: ");
            userInput = sc2.next().charAt(0);
        } while (userInput == 'y');
    }
}