<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
//exit(print_r($_POST));

//exit(print_r($_POST)); //display $_POST array values from form

// or, for nicer display in browser...
/* echo "<pre>";
print_r($_POST);
echo "</pre>";
exit(); //stop processing, otherwise, errors below */


//After testing, comment out above lines.

//code to process inserts goes here

//get form data               


class Person {
                
    private $fname;
    private $lname;
    private $age;

    public function __construct($fn = "John", $ln = "Doe", $ag = 21)
    {
        $this->fname= $fn;
        $this->lname = $ln;
        $this->age= $ag;
        echo("creating <strong>" . $this->fname . " " . $this->lname . " is " . $this->age . "</strong> person object from parameterized constructor");
    }

    public function __destruct()
    {
        echo("destroying <strong>" . $this->fname . " " . $this->lname . " is " . $this->age . "</strong> person object");
    }

    //set
    public function SetFname($fn= "Jane")
    {
        $this->fname = $fn;
    }
    public function SetLname($ln = "Doe")
    {
        $this->lname = $ln;
    }
    public function SetAge($ag = "21")
    {
        $this->age = $ag;
    }

    //get
    public function GetFname()
    {
        return $this->fname;
    }
    public function GetLname()
    {
        return $this->lname;
    }
    public function GetAge()
    {
        return $this->age;
    }
    
}
?>
