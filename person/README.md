> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Daniel Jaramillo

### Assignment 4 Requirements:

1. Successfully set-up LIS4381 Portal Page
2. Set up individual assignment pages to reflect assignment details
3. Update carousel on home page to reflect developer
4. Add jQuery validation and regular expression attributes to Assignment 4
5. Complete Skillsets 10-12

#### README.md file should include the following items:

* Screenshot of LIS4381 Website Homepage
* Failed validation for A4
* Successful validation for A4
* Skillsets 10-12

#### Assignment Screenshots:


Website Link [Here.](http://localhost/repos/lis4381/index.php)


Home Page:
![Home Page](Images/homepage.png) 
Failed Validation:
![Failed Validation](Images/failed.png)
Successful Validation:
![Successful Validation](Images/success.png)


| Skillset 10 | Skillset 11 | Skillset 12 |
| --- | --- | --- |
| ![Skillset 10](Images/skillset10.png) | ![Skillset 11](Images/skillset11.png) | ![Skillset 12](Images/skillset12.png) |
| --- | --- | --- |