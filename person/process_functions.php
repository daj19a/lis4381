<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
//exit(print_r($_POST));


//include('index.php'); //forwarding is faster, one trip to server

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Assignment 4 Website.">
	<meta name="author" content="Daniel Jaramillo">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - In Class Assignment</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>
    <div class="container">
        <div class="starter template">
            <div class="page-header">
                <?php include_once("global/header.php"); ?>
            </div>
            <?php
            require_once("process.php");
            $personFname = $_POST['fname'];
            $personLname = $_POST['lname'];
            $personAge = $_POST['age'];
            $person1 = new Person();
            $person2 = new Person($personFname, $personLname, $personAge);
            ?>

            <div class="table-responsive">
                <table id="myTable" class="table table-striped table-condensed" >
            <thead>
            <tr>
            <th>Fname</th>
            <th>Lname</th>
            <th>Age</th>
            </tr>
            </thead>
            <tr>
                <td><?php echo $person1->GetFname() ?></td>
                <td><?php echo $person1->GetLname() ?></td>
                <td><?php echo $person1->GetAge()?></td>
            </tr>
            <tr>
                <td><?php echo $person2->GetFname() ?></td>
                <td><?php echo $person2->GetLname() ?></td>
                <td><?php echo $person2->GetAge()?></td>
            </tr>
            </p>
            <?php include_once "global/footer.php"; ?>
                </table>
            </div> <!-- table -->
        </div>
        </div>

        <?php include_once("../js/include_js.php"); ?>

<script type="text/javascript">
$(document).ready(function(){
 $('#myTable').DataTable({
         responsive: true,				 
         //https://datatables.net/reference/option/lengthMenu
         //1st inner array: number of actual records displayed
         //2nd inner array: number listed in the drop-down menu
         //Note: -1 is used to disable pagination (i.e., display all rows), use with "All"
         //Note: pageLength property automatically set to first value given in array: here, 10
         "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
//permit sorting (i.e., no sorting on last two columns: delete and edit)
 });
});
</script>
</body>
</html>