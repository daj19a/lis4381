<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Assignment 4 Website.">
	<meta name="author" content="Daniel Jaramillo">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - In Class Assignment</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
					</div>

					<h2>Person Class</h2>

						<form class="form-horizontal" role="form" method="post" action="process_functions.php">

								<!--num1 and num2 -->
								<div class="form-group">
									<label class="control-label col-sm-2" for="fname">First Name</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="fname" id="fname" placeholder ="Enter first name">
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-sm-2" for="lname">Last Name:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="lname" id="lname" placeholder ="Enter last name">
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-sm-2" for="age">Age:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="age" id="age" placeholder ="Enter age">
									</div>
								</div>

								<!-- button -->
								<div class="form-group">
									<div class="col-sm-12">
										<button type="submit" class="btn btn-default">Submit</button>
									</div>
								</div>
						</form>

			<?php include_once "global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<?php include_once("../js/include_js.php"); ?>

<script type="text/javascript">
 //See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7

</script>

</body>
</html>
