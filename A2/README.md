> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Daniel Jaramillo

### Assignment 2 Requirements:

1. Develop Healthy Recipes Application in Android Studios
2. Successfully compile and run the application
2. Complete Skillsets 1-3

#### README.md file should include the following items:

* Screenshot of first user interface.
* Screenshot of second user interface.
* Screenshots of Skillsets 1-3.

#### Assignment Screenshots:

| First User Interface | Second User Interface |
| --- | --- |
| ![First User Interface](Images/interface1.png) | ![Second User Interface](Images//interface2.png) |
| --- | --- |

| Skillset 1 | Skillset 2 | Skillset 3 |
| --- | --- | --- |
| ![Skillset 1](Images/skillset1.png) | ![Skillset 2](Images/skillset2.png) | ![Skillset 3](Images/skillset3.png) |
| --- | --- | --- |
