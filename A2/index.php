<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Assignment 4 Website">
		<meta name="author" content="Daniel Jaramillo">
    <link rel="icon" href="favicon.png">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Students were expected to develop the Healthy Recipes application in Android Studio. Students were also expected to complete Skillsets 1-3, Java programs meant to test understanding. 
				</p>

				<h4>Healthy Recipes Interface 1</h4>
				<img src="Images/interface1.png" class="img-responsive center-block" alt="Healthy Recipes Interface 1">

				<h4>Healthy Recipes Interface 2</h4>
				<img src="Images/interface1.png" class="img-responsive center-block" alt="Healthy Recipes Interface 2">

				<h4>Skillset 1</h4>
				<img src="Images/skillset1.png" class="img-responsive center-block" alt="Skillset 1">

				<h4>Skillset 2</h4>
				<img src="Images/skillset2.png" class="img-responsive center-block" alt="Skillset 2">

				<h4>Skillset 3</h4>
				<img src="Images/skillset3.png" class="img-responsive center-block" alt="Skillset 3">
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
